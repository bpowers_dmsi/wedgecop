import depcheck from 'depcheck';
import readPkg from 'read-pkg';

export type Results = {
  [projectName: string]: depcheck.Results;
};

const getOptions = (opts: Options): depcheck.Options => {
  return {
    ignoreMatches: opts.ignores?.split(',') ?? [],
    ignorePatterns: opts['ignore-patterns']?.split(',') ?? [],
  };
};

const checkDependencies = async (projectPaths: string[], opts: any) => {
  const options = getOptions(opts);
  const allResults: Results = {};
  for (const projectPath of projectPaths) {
    const packageJson = await readPkg({
      cwd: projectPath,
    });
    const result = await depcheck(projectPath, options);
    allResults[packageJson.name] = result;
  }
  return allResults;
};

export default checkDependencies;
