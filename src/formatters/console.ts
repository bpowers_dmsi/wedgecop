import chalk from 'chalk';
import pluralize from 'pluralize';
import { getBorderCharacters, table } from 'table';

import { Results as DepCheckResults } from '../depcheck/checkDependencies';
import { Results as VersionResults } from '../utils/checkVersions';

const format = (depCheck: DepCheckResults, versionResult: VersionResults, opts: Options) => {
  let output = '\n';
  let unusedCount = 0;
  let missingCount = 0;
  let mismatchCount = 0;

  // Depcheck results
  Object.keys(depCheck).forEach((projectName) => {
    const { dependencies, devDependencies, missing } = depCheck[projectName];
    if (
      (opts.unused && (dependencies.length || devDependencies.length)) ||
      (opts.missing && Object.keys(missing).length)
    ) {
      unusedCount += dependencies.length + devDependencies.length;
      missingCount += Object.keys(missing).length;
      
      output += `${chalk.underline(projectName)}\n`;
      
      const outputTable: string[][] = [];
      
      if (opts.missing && Object.keys(missing).length) {
        outputTable.push([chalk.red('Missing'), Object.keys(missing).join('\n')]);
      }
      
      if (opts.unused && (dependencies.length || devDependencies.length)) {
        outputTable.push([chalk.yellow('Unused'), [...dependencies, ...devDependencies].join('\n')]);
      }
      
      if (outputTable.length) {
        output += `${table(outputTable, {
          border: getBorderCharacters('void'),
          drawHorizontalLine: () => false,
        })}\n`;
      }
    }
  });

  // Version check results
  if (Object.keys(versionResult).length) {
    mismatchCount += Object.keys(versionResult).length;

    output += 'Version Mismatches\n';

    const outputTable: string[][] = [];

    outputTable.push([chalk.white('Package'), chalk.white('Version'), chalk.white('Used in project')]);

    Object.keys(versionResult).forEach((packageName) => {
      const versions = versionResult[packageName];

      outputTable.push([
        chalk.red(packageName),
        chalk.magenta(
          Object.keys(versions)
            .reduce<string[]>((allVersions, versionNumber) => {
              return [...allVersions, ...versions[versionNumber].map((_projects, i) => (i === 0 ? versionNumber : ''))];
            }, [])
            .join('\n')
        ),
        Object.keys(versions)
          .reduce<string[]>((allProjects, versionNumber) => {
            return [...allProjects, ...versions[versionNumber]];
          }, [])
          .join('\n'),
      ]);
    });

    output += `${table(outputTable, {})}\n`;
  }

  const total = (opts.unused ? unusedCount : 0) + (opts.missing ? missingCount : 0) + mismatchCount;

  if (total > 0) {
    const detailedOutput = [];
    if (opts.unused) {
      detailedOutput.push(`${unusedCount} unused`);
    }
    if (opts.missing) {
      detailedOutput.push(`${missingCount} missing`);
    }
    if (opts.mismatched) {
      detailedOutput.push(`${mismatchCount} version ${pluralize('mismatch', mismatchCount)}`);
    }
    output += chalk.red.bold(`\u2716 ${total} ${pluralize('problem', total)} (${detailedOutput.join(', ')})\n`);
  }

  return total > 0 ? chalk.reset(output) : '';
};

export default format;
