#!/usr/bin/env node
import path from 'path';
import readPkgUp from 'read-pkg-up';
import sade from 'sade';

import getMonorepoProjects from './utils/getMonorepoProjects';
import getUserPaths from './utils/getUserPaths';
import checkDependencies from './depcheck/checkDependencies';
import checkVersions from './utils/checkVersions';
import consoleFormatter from './formatters/console';

const cwd = process.cwd();

const version = readPkgUp.sync({ cwd: __dirname })?.packageJson.version ?? '0.0.0';

const prog = sade('wedgecop', true);
prog
  .version(version)
  .describe('Dependency checking for monorepos.')
  .option('-m --monorepo-config', 'Specify a repo configuration [lerna | yarn | auto]', 'auto')
  .option('-o --output-file', 'Specify file to write report to.  Outputs to the console if not specified.')
  .option('-f --format', 'Use a specific output file format', 'junit')
  .option('--missing', 'Check for missing dependencies', true)
  .option('--unused', 'Check for unused dependencies', true)
  .option('--mismatched', 'Check for package version mismatches', true)
  .option('--ignores', 'A comma separated array containing package names to ignore. It can be glob expressions')
  .option(
    '--ignore-patterns',
    'Comma separated patterns describing files to ignore. Patterns must match the .gitignore spec'
  )
  .example("'public/*' 'private/*'")
  .action(async (opts) => {
    let paths = opts._.length
      ? await getUserPaths(opts._, cwd)
      : await getMonorepoProjects(opts['monorepo-config'], cwd);
    if (!paths?.length) {
      console.log('No projects found.');
      process.exitCode = 1;
      return;
    }

    const fullPaths = paths.map((currentPath) => path.join(cwd, currentPath));

    const depCheckResults = opts.missing || opts.unused ? await checkDependencies(fullPaths, opts) : {};
    const versionCheckResults = opts.mismatched ? await checkVersions(fullPaths) : {};

    const formatter = !!opts.o ? () => {} : consoleFormatter;
    console.log(formatter(depCheckResults, versionCheckResults, opts));
  })
  .parse(process.argv);
