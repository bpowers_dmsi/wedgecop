import globby from 'globby';

const getUserPaths = async (userGlobs: string[], cwd?: string) => {
  return await globby(userGlobs, {
    cwd,
    onlyDirectories: true,
  });
};

export default getUserPaths;
