import readPkg from 'read-pkg';

type ProjectDependencies = {
  [projectName: string]: {
    dependencies: { [name: string]: string };
    devDependencies: { [name: string]: string };
  };
};

type PackageVersions = {
  [version: string]: string[];
};

export type Results = {
  [packageName: string]: PackageVersions;
};

const getDependencies = async (directories: string[]) => {
  return await directories.reduce<Promise<ProjectDependencies>>(async (acc, directory) => {
    const projectDeps = await acc;
    const packageJson = await readPkg({
      cwd: directory,
    });

    return {
      ...projectDeps,
      [packageJson.name]: {
        dependencies: packageJson.dependencies as { [name: string]: string },
        devDependencies: packageJson.devDependencies as {
          [name: string]: string;
        },
      },
    };
  }, Promise.resolve({}));
};

const createPackageVersionMap = (projectDependencies: ProjectDependencies) => {
  return Object.keys(projectDependencies).reduce<{
    [key: string]: PackageVersions;
  }>((acc, projectName) => {
    const dependencySections = [
      projectDependencies[projectName].dependencies,
      projectDependencies[projectName].devDependencies,
    ];
    dependencySections.forEach((section) => {
      Object.keys(section ?? {}).forEach((depName) => {
        const depVersion = section[depName];
        let referencedVersions = acc[depName];
        if (!referencedVersions) {
          referencedVersions = {};
          acc[depName] = referencedVersions;
        }
        let referencingProjects = referencedVersions[depVersion];
        if (!referencingProjects) {
          referencingProjects = [];
          referencedVersions[depVersion] = referencingProjects;
        }
        if (!referencingProjects.includes(projectName)) {
          referencingProjects.push(projectName);
        }
      });
    });
    return acc;
  }, {});
};

const checkVersions = async (paths: string[]) => {
  const allDependencies = await getDependencies(paths);
  const packageVersionMap = createPackageVersionMap(allDependencies);
  return Object.keys(packageVersionMap).reduce<{
    [packageName: string]: PackageVersions;
  }>((filteredPackages, packageName) => {
    const versionsArray = packageVersionMap[packageName];
    if (Object.keys(versionsArray).length > 1) {
      return { ...filteredPackages, [packageName]: versionsArray };
    }
    return filteredPackages;
  }, {});
};

export default checkVersions;
