import catchify from 'catchify';
import globby from 'globby';
import loadJsonFile from 'load-json-file';
import path from 'path';
import readPkg from 'read-pkg';

type LernaConfig = {
  packages?: string[];
};

type MonorepoConfig = 'lerna' | 'yarn' | 'auto';

const getLernaProjects = async (cwd: string) => {
  const [error, lernaConfig] = await catchify(loadJsonFile<LernaConfig>(path.join(cwd, 'lerna.json')));
  if (error || !lernaConfig?.packages?.length) {
    return null;
  }
  return await globby(lernaConfig.packages, {
    cwd: cwd,
    onlyDirectories: true,
  });
};

const getWorkspaces = async (cwd: string) => {
  const [error, packageJson] = await catchify(readPkg({ cwd: cwd }));
  if (error || !packageJson?.workspaces?.packages?.length) {
    return null;
  }

  return await globby(packageJson.workspaces.packages, {
    cwd: cwd,
    onlyDirectories: true,
  });
};

const getMonorepoProjects = async (config: MonorepoConfig, cwd?: string) => {
  const lerna = config === 'lerna' || config === 'auto';
  const yarn = config === 'yarn' || config === 'auto';
  const userCwd = cwd ?? process.cwd();
  return (lerna && (await getLernaProjects(userCwd))) || (yarn && (await getWorkspaces(userCwd))) || [];
};

export default getMonorepoProjects;
