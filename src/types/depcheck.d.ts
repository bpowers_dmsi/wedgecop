import 'depcheck';

declare module 'depcheck' {
  interface Options {
    ignoreBinPackage?: boolean;
    skipMissing?: boolean;
    ignoreDirs?: ReadonlyArray<string>;
    ignoreMatches?: ReadonlyArray<string>;
    ignorePatterns?: ReadonlyArray<string>;
    package?: {
      dependencies?: PackageDependencies;
      devDependencies?: PackageDependencies;
      peerDependencies?: PackageDependencies;
      optionalDependencies?: PackageDependencies;
    };
    parsers?: {
      [match: string]: Parser;
    };
    detectors?: ReadonlyArray<Detector>;
    specials?: ReadonlyArray<Parser>;
  }
}
