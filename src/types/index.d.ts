type Options = {
  'ignore-patterns'?: string;
  ignores?: string;
  missing: boolean;
  unused: boolean;
  mismatched: boolean;
};
